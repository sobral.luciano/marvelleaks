import { BrowserRouter, Switch, Route } from 'react-router-dom';
import React from 'react';

import { Search } from './pages/search';
import { Character } from './pages/character';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Search} />
        <Route path="/character/:id" component={Character} />
      </Switch>
    </BrowserRouter>
  );
}
