import React from 'react';
import GlobalStyle from './style/global';
import Routes from './router';

function App() {
  return (
    <>
      <GlobalStyle />
      <Routes />
    </>
  );
}

export default App;
