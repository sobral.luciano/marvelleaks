import { createGlobalStyle } from 'styled-components';

const whiteMarvel = '#fff';

export default createGlobalStyle`

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    border: 0;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box
  }

  *:focus {
    outline: 0;
  }

  html {
    font-size: 62.5%;
    height: 100vh;
    width: 100vw;
  }

  body {
    height: 100vh;

    color: ${whiteMarvel};

    text-rendering: optimizeLegibility;
    -moz-font-smoothing: antialiased;
    -webkit-font-smoothing: antialiased;

    font-style: normal;
    font-size: 1.6rem;
    line-height: 1.1rem;
    font-family: RobotoCondensed Bold, Trebuchet MS, Helvetica, Arial, sans-serif;
    font-kerning: auto;
    letter-spacing: 0.1rem;
    text-transform: uppercase;
  }
`;
