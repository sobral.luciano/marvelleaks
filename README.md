 # SoftMarvel - The Plan

  Segue abaixo as instruções para próxima fase do processo seletivo:

  Crie um projeto utilizando React e a API MarvelQL (https://api.marvelql.com/) seguindo as especificações abaixo:

  OBS.: Caso a MarvelQL esteja indisponível, utilize a API oficial da Marvel (https://developer.marvel.com/docs#)


  Funcionalidades esperadas:

  [ ] Crie uma lista de cards para exibir os personagens mostrando a imagem e o nome;

  [ ] Possibilite o usuário buscar personagens;

  [ ] Na lista o usuário pode ir para a página de detalhes do personagem e ver a lista de series dele;

  [ ] Crie um formulário para editar um personagem Marvel (salvando apenas no client-side);


  Restrições técnicas:

  [ ] Utilize o create-react-app como base;

  [ ] Utilize redux para gerenciar o estado;

  [ ] Utilize react-router para trocar de página;

  [ ] Utilize @testing-library/react para testes;


  Diferencial:

  [ ] Crie uma pipeline no GitLab; (Exemplo: build => test => deploy);

  [ ] Substituir o redux pelo Local state management do Apollo Client (caso esteja utilizando a MarvelQL);